package at.bxm.docker.groovyweb

import spock.lang.Specification

import static at.bxm.docker.groovyweb.WebServer.mimeType

class WebServerTest extends Specification {

    def "determine MIME types"() {
        expect:
        mimeType(new File("file.ghtml")) == "text/html"
        mimeType(new File("file.html")) == "text/html"
        mimeType(new File("file.xml")) == "application/xml"
        mimeType(new File("file.gif")) == "image/gif"
        mimeType(new File("file.jpg")) == "image/jpeg"
        mimeType(new File("file.png")) == "image/png"
        mimeType(new File("file.js")) == "text/javascript"
        mimeType(new File("file.css")) == "text/css"
    }

    def "MIME types for strange filenames"() {
        expect:
        mimeType(new File("file")) == "application/octet-stream"
        mimeType(new File("file.")) == "application/octet-stream"
        mimeType(new File(".")) == "application/octet-stream"
        mimeType(new File(".1")) == "application/octet-stream"
        mimeType(new File("1.")) == "application/octet-stream"
        mimeType(new File("..")) == "application/octet-stream"
        mimeType(new File("")) == "application/octet-stream"
    }
}