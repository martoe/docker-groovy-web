package at.bxm.docker.groovyweb

import fi.iki.elonen.NanoHTTPD
import groovy.text.SimpleTemplateEngine
import groovy.util.logging.Log4j2

import static fi.iki.elonen.NanoHTTPD.Response.Status.*

@Log4j2
class WebServer extends NanoHTTPD {

    static final int port = 5050
    final String webserverRootDir
    final String webserverTemplatesDir

    WebServer(String webserverRootDir = "/www/root", String webserverTemplatesDir = "/www/templates") {
        super(port)
        this.webserverRootDir = webserverRootDir
        this.webserverTemplatesDir = webserverTemplatesDir
    }

    @Override
    void start() throws IOException {
        start(SOCKET_READ_TIMEOUT, false)
        log.info "Listening at port $port"
    }

    @Override
    Response serve(String uri, Method method, Map<String, String> headers,
                   Map<String, String> params, Map<String, String> files) {
        try {
            if (method != Method.GET) {
                log.warn "Method not allowed: $method $uri"
                return error(METHOD_NOT_ALLOWED)
            } else if (uri.endsWith(".ghtml")) {
                log.debug "Serving template: $uri"
                def model = [:]
                model.put("params", params)
                model.put("headers", headers)
                model.put("method", method)
                model.put("uri", uri)
                def template = new SimpleTemplateEngine().createTemplate("$webserverTemplatesDir$uri" as File).make(model)
                return newFixedLengthResponse(OK, MIME_HTML, template.toString())
            } else {
                log.debug "Serving content: $uri"
                return getData(uri)
            }
        } catch (Exception e) {
            log.error("$e ($method $uri)", e)
            return error(INTERNAL_ERROR)
        }
    }

    private static Response error(Response.IStatus status) {
        newFixedLengthResponse(status, MIME_PLAINTEXT, status.description)
    }

    private Response getData(String uri) {
        def file = new File("$webserverRootDir$uri")
        if (file.isDirectory()) {
            file = new File(file, "index.html")
        }
        if (file.exists()) {
            return newFixedLengthResponse(OK, mimeType(file), file.text)
        } else {
            log.warn "Not found: $file.absolutePath"
            return error(NOT_FOUND)
        }
    }

    static String mimeType(File file) {
        def type = URLConnection.guessContentTypeFromName(file.name)
        if (type) {
            return type
        }
        def fileExtPos = file.name.lastIndexOf(".")
        def fileExt = file.name.substring(fileExtPos + 1)
        switch (fileExt) {
            case "ghtml": return "text/html"
            case "js": return "text/javascript"
            case "css": return "text/css"
        }
        log.warn "Could not determine MIME type for '$file.name'"
        return "application/octet-stream"
    }
}
