docker-groovy-web
=================

A Docker image with a simple webserver that serves [Groovy templates](http://groovy-lang.org/templating.html#_simpletemplateengine).

The webserver data is provided with four mountpoints:

* `/www/root/` for any static content
* `/www/templates/` for Groovy templates (each request that ends with ".ghtml" will be redirected to this location)
* `/groovy/src/` for additional Groovy class files
* `/groovy/jars/` for additional Java libraries

To overwrite the provided [Log4J2 configuration](https://logging.apache.org/log4j/2.x/manual/configuration.html), simple create a `log4j2.xml` file and mount it into the `/groovy/src/` directory.



Building the image
------------------

```
./gradlew clean build
```



Pushing the image
-----------------


Prepare:
```
export VERSION=1.0
export VERSION_NEXT=1.1-SNAPSHOT
docker login --username=martoe
```
Execute:
```
sed -i 's/version=.*/version=$VERSION/' gradle.properties && \
git commit -am"Release $VERSION" && \
./gradlew clean publish && \
git tag -am"Release $VERSION" $VERSION && \
sed -i 's/version=.*/version=$VERSION_NEXT/' gradle.properties && \
git commit -am"Post-release $VERSION" && \
git push --tags origin master
```
The image is now available at the [Docker Hub](https://hub.docker.com/r/martoe/docker-groovy-web).



Running an example website
--------------------------

```
docker run --rm -p 5050:5050 \
  -v $(pwd)/example/static:/www/root:ro \
  -v $(pwd)/example/templates:/www/templates:ro \
  -v $(pwd)/example/groovy:/groovy/src:ro \
  -v $(pwd)/example/libs:/groovy/jars:ro \
  martoe/docker-groovy-web:1.0-SNAPSHOT
```
Website is available at http://localhost:5050
